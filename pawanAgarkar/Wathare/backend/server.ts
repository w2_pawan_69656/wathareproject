import express, { Request, Response } from 'express';
import mongoose from 'mongoose';
import cors from 'cors';
import Data from './data.model';
import { getData } from './data.controller';

const app = express();
const port = 5000;

app.use(cors());

app.get('/api/data', getData);

mongoose
  .connect('mongodb://127.0.0.1:27017/myapp', {
    // useNewUrlParser: true,
    // useUnifiedTopology: true,
  });
  app.get('/api/data', async (req: Request, res: Response) => {
    try {
      const data = await Data.find({});
      console.log('Fetched data:', data);
      res.json(data);
    } catch (error) {
      res.status(500).json({ message: 'Error fetching data' });
    }
  });
    app.listen(port, () => {
      console.log(`Server running on port ${port}`);
    });
 