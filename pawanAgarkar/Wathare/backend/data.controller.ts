import { Request, Response } from 'express';
import DataModel from './data.model';

export const getData = async (req: Request, res: Response): Promise<void> => {
  try {
    const data = await DataModel.find().sort({ timestamp: -1 }).limit(50);
    res.status(200).json(data);
  } catch (error) {
    res.status(500).json({ error: 'Internal server error' });
  }
};