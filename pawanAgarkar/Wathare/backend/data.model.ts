import { Schema, model, Document } from 'mongoose';

interface IData extends Document {
  timestamp: Date;
  value: number;
}

const DataSchema = new Schema({
  timestamp: { type: Date, required: true },
  value: { type: Number, required: true },
});

const DataModel = model<IData>('Data', DataSchema);

export default DataModel;